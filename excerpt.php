<?php 

	$text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

	function excerpt($text, $limit)
	{
		$text = htmlspecialchars(trim($text));

		if(is_numeric($text) || strlen($text) <= $limit)
		{
			$excerpt = $text <= '' || $text == null ? 'NULL' : $text;
		}
		else
		{
			$excerpt = $text == '' ? 'NULL' : substr($text, 0, $limit) . ' ...';
		}
		return $excerpt;
	}

	$text = excerpt($text, 50);

	echo $text;
?>